package io.gitlab.z9001.snapp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by zoraver on 10/14/16.
 */
public class MainActivity extends Activity {

    ArrayList<ApplicationInfo> appInfoList;
    ArrayList<ResolveInfo> resolveInfoList;

    ListView appList;
    PackageManager pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize the user interface
        this.setContentView(R.layout.activity_main);

        this.appList = (ListView) this.findViewById(R.id.appList);
        this.appList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent launchIntent = pm.getLaunchIntentForPackage(resolveInfoList.get(position).activityInfo.packageName);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_RETAIN_IN_RECENTS);
                startActivity(launchIntent);
            }
        });
        this.pm = this.getPackageManager();
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<ResolveInfo> newResolveInfoList = (ArrayList<ResolveInfo>) this.pm.queryIntentActivities(getFilterIntent(), PackageManager.MATCH_ALL);
        if(this.resolveInfoList == null || !this.resolveInfoList.containsAll(newResolveInfoList) || !newResolveInfoList.containsAll(this.resolveInfoList)) {
            this.resolveInfoList = newResolveInfoList;
            this.appList.setAdapter(new ArrayAdapter<ResolveInfo>(this, android.R.layout.simple_list_item_1, android.R.id.text1, this.resolveInfoList){
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    ((TextView) view.findViewById(android.R.id.text1)).setText(pm.getApplicationLabel(this.getItem(position).activityInfo.applicationInfo));
                    return view;
                }
            });
        }
    }

    private Intent getFilterIntent() {
        Intent filterIntent = new Intent(Intent.ACTION_MAIN);
        filterIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        return filterIntent;
    }
}
